#ifndef _SCHALTUNG_H_
#define _SCHALTUNG_H_
#define _USE_MATH_DEFINES

#include "Kondensator.h"
#include "Spule.h"
#include "Widerstand.h"
#include <iostream>
#include <fstream>
#include <string>
#include <math.h>

using namespace std;

class Schaltung {
private:
    Widerstand R1;
    Spule L1;
    Kondensator C1;
    vector<float> uaStory;
	fstream inFile;
	fstream outFile;
    float frequenz;

public:
    Schaltung(Widerstand, Spule, Kondensator,float);
    float calcUa(float);
	void writeUa(float);
	void simulate();
	~Schaltung();
    void saveStory(float);
	void info();
    vector<float> getUaStory();
    float getSinusUe(float);
    Widerstand getWiderstand();
    Spule getSpule();
    Kondensator getKondensator();
};

#endif
