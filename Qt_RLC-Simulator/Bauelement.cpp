#include "Bauelement.h"
#include <iostream>

using namespace std;

void Bauelement::addUtoStore(float u) {
	ustore.push_back(u);
}

void Bauelement::addItoStore(float strom) {
	istore.push_back(strom);
}

vector<float> Bauelement::getUStore(){
 return ustore;
}
vector<float> Bauelement::getIStore(){
    return istore;
}

void Bauelement::info() {
	cout << "Bauelement" << endl;
}
