#include "Spule.h"
#include <iostream>

float Spule::calcDeltaU(float strom) {
	return induktivitšt*strom;
}

float Spule::calcNextI(float spannung) {
    return spannung / induktivitšt;
}

Spule::Spule(float induk) {
	induktivitšt = induk;
}

void Spule::info() {
	std::cout << "Spule" << std::endl;
}
