#ifndef _WIDERSTAND_H_
#define _WIDERSTAND_H_

#include "Bauelement.h"

class Widerstand : public Bauelement{
private:
    float widerstand = 1;
public:
	Widerstand(float);
	Widerstand():Widerstand(0) {};
	float calcDeltaU(float);
	void info();

};

#endif
