#include "Schaltung.h"

Schaltung::Schaltung(Widerstand R, Spule L, Kondensator C, float freq) {
    R1 = R;
    L1 = L;
    C1 = C;

    frequenz = freq;
}

void Schaltung::writeUa(float ua) {
    uaStory.push_back(ua);
}

float Schaltung::calcUa(float ue) {
    float i_l = (L1.getIStore().size() < 1 ? 0 : L1.getIStore().back());

    float i_r = i_l;
    float u_r = R1.calcDeltaU(i_r);
    R1.addUtoStore(u_r);
    R1.addItoStore(i_r);

    float u_c = (C1.getUStore().size() < 1 ? 0 : C1.getUStore().back());
    float u_l = ue - R1.getUStore().back() - (u_c);
    L1.addUtoStore(u_l);

    float i_c = i_l;
    float ua = u_c;
    u_c = u_c + (C1.calcDeltaU(i_c));
    C1.addUtoStore(u_c);
    C1.addItoStore(i_c);

    i_l = i_l + L1.calcNextI(u_l);
    L1.addItoStore(i_l);

    return ua;
}


float Schaltung::getSinusUe(float zeitpunkt){
    return (float) sin(2*M_PI*frequenz*zeitpunkt);
}

void Schaltung::simulate() {
    float ue = 0;
	float ua = 0;
    float zeitpunkt = 0;
	do {
        ue = getSinusUe(zeitpunkt);
        zeitpunkt+=0.01;
        ua = calcUa(ue);

        writeUa(ua);

    } while (zeitpunkt < 4*M_PI);
}

Schaltung::~Schaltung() {
	outFile.close();
	cout << "Schaltung destroyed" << endl;
}

vector<float> Schaltung::getUaStory(){
    return uaStory;
}


Widerstand Schaltung::getWiderstand(){
    return R1;
}

Spule Schaltung::getSpule(){
    return L1;
}

Kondensator Schaltung::getKondensator(){
    return C1;
}
