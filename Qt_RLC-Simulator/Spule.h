#ifndef _SPULE_H_
#define _SPULE_H_

#include "Bauelement.h"

class Spule : public Bauelement {
private:
    float induktivitšt = 1;

public:
	float calcDeltaU(float);
	float calcNextI(float);
	Spule(float);
	Spule() :Spule(0) {};
	void info();
};

#endif
