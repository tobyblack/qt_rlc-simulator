#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <vector>
#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include <QGraphicsSimpleTextItem>
#include <QLineEdit>
#include <QString>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


private slots:
    void on_pushStart_clicked();

    void on_resetButton_clicked();

private:
    Ui::MainWindow *ui;
    QGraphicsScene scene;
    void drawLine(QPen, std::vector<float>);
    void clearScene();
};

#endif // MAINWINDOW_H
