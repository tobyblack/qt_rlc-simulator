#pragma once

#ifndef _KONDENSATOR_H_
#define _KONDENSTAOR_H_

#include <vector>
#include "Bauelement.h"

class Kondensator : public Bauelement {
private:
    float kapazitšt = 1;

public:
	float calcDeltaU(float);
	Kondensator(float);
	Kondensator() :Kondensator(0) {};
	void info();
};

#endif
