#pragma once

#ifndef _BAUELEMENT_H_
#define _BAUELEMENT_H_

#include <vector>

using namespace std;

class Bauelement {
protected:
	vector<float> ustore;
	vector<float> istore;


public:
	void addUtoStore(float);
	void addItoStore(float);
    vector<float> Bauelement::getUStore();
    vector<float> Bauelement::getIStore();
	virtual void info();
};

#endif
