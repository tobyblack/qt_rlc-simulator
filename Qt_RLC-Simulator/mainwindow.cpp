#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "Schaltung.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushStart_clicked()
{
    float widerstand = std::stof(ui->widerstandEdit->text().toStdString());
    float induktivitaet = std::stof(ui->indukEdit->text().toStdString());
    float kapazitaet = std::stof(ui->kapaEdit->text().toStdString());
    float frequenz = std::stof(ui->freqEdit->text().toStdString());

    Widerstand r1(widerstand);
    Spule l1(induktivitaet);
    Kondensator c1(kapazitaet);

    Schaltung schaltung(r1, l1, c1, frequenz);
    schaltung.simulate();

    //Stifte zum Zeichnen anlegen: Farbe,Stärke,Muster
    QPen beschriftung(Qt::black,2,Qt::SolidLine);
    QPen uaLinie(Qt::blue,2,Qt::SolidLine);
    QPen ruLinie(Qt::red,2,Qt::SolidLine);
    QPen luLinie(Qt::green,2,Qt::SolidLine);
    QPen cuLinie(Qt::black,2,Qt::SolidLine);

    //Achsen für den Graphen Zeichnen: X1Y1,X2Y2
    QGraphicsLineItem* x_Achse = new QGraphicsLineItem(0,140,750,140);
    QGraphicsLineItem* y_Achse = new QGraphicsLineItem(0,0,0,280);

    //Farbe der Linie wählen
    x_Achse->setPen(beschriftung);
    y_Achse->setPen(beschriftung);

    //Beide Achsen auf die Szene zeichen
    scene.addItem(x_Achse);
    scene.addItem(y_Achse);

    drawLine(uaLinie, schaltung.getUaStory());
    drawLine(ruLinie, schaltung.getWiderstand().getUStore());
    drawLine(luLinie, schaltung.getSpule().getUStore());
    drawLine(cuLinie, schaltung.getKondensator().getUStore());

}

void MainWindow::drawLine(QPen linie, vector<float> numbers){
    double d_X1 = 0;
    double d_X2 = 0;
    double d_Y1 = 0;
    double d_Y2 = 0;
    double skalierung = 0;

    double max = 0;
    int breakpoint = 750;
    for(int i = 0; i < numbers.size(); i++){
        if(numbers.at(i) > max){
            max = numbers.at(i);
        }
        if(isinf(numbers.at(i))){
            breakpoint = i;
            break;
        }
    }

    skalierung = 140/max;

    for(int i = 2; i < breakpoint; i++){
        if(isinf(numbers.at(i))){
            break;
        }

        d_X1 = (i-1);
        d_X2 = i;

        d_Y1 = (skalierung*max) - (skalierung*numbers.at(i-1));
        d_Y2 = (skalierung*max) - (skalierung*numbers.at(i));

        scene.addLine(d_X1,d_Y1,d_X2,d_Y2,linie);
    }

    ui->graphicsView->setScene(&scene);
}

void MainWindow::clearScene(){
    scene.clear();
}

void MainWindow::on_resetButton_clicked()
{
    clearScene();
}
