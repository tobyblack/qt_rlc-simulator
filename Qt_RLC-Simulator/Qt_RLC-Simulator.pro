#-------------------------------------------------
#
# Project created by QtCreator 2016-05-27T16:23:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Qt_RLC-Simulator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Bauelement.cpp \
    Kondensator.cpp \
    Schaltung.cpp \
    Spule.cpp \
    Widerstand.cpp

HEADERS  += mainwindow.h \
    Bauelement.h \
    Kondensator.h \
    Schaltung.h \
    Spule.h \
    Widerstand.h

FORMS    += mainwindow.ui
