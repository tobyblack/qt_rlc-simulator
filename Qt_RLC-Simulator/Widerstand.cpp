#include "Widerstand.h"
#include <iostream>

float Widerstand::calcDeltaU(float strom) {
	return widerstand*strom;
}

Widerstand::Widerstand(float wider) {
	widerstand = wider;
}

void Widerstand::info() {
	std::cout << "Widerstand" << std::endl;
}